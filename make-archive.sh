#!/usr/bin/env bash

set -e

# packer=TAR
packer=ZIP

# repo_src_param=local

proceed() {
    [ -n "$(cat config/$1)" ] || return

    name=${1%.*}
    after_clone_cmd=
    cleanup_files=
    before_cleanup_cmd=
    before_pack_cmd=

    source ./config/$1

    echo Processing repo $name:

    [ -n "$branch" ] || branch=master

    [ "$repo_src_param" == 'local' ] &&
        repo_src=$HOME/Devel/Eleven/$name ||
        repo_src=git@bitbucket.org:guildone/$name.git

    echo "=> Clone"
    git clone --branch $branch $repo_src || { echo FAIL; exit 1; }
    $after_clone_cmd

    echo "=> Cleanup"
    cd $name
    eval "$before_cleanup_cmd" || { echo FAIL; exit 1; }
    rm -rf .git $cleanup_files
    cd - >/dev/null

    echo "=> Pack"
    $before_pack_cmd

    # mv $name packs/${name}_${lang_name}_${lang_ver}
    arch_name=packs/$(date +%F)_${name}_${lang_name}_${lang_ver}_git-clone

    if [ "$packer" == "ZIP" ]; then
        arch_name=$arch_name.zip
        [ -z "$PACK_PASSWORD" ] &&
            pack_cmd="zip -rqy $arch_name $name" ||
            pack_cmd="zip -rqyeP $PACK_PASSWORD $arch_name $name"
    elif [ "$packer" == "TAR" ]; then
        arch_name=$arch_name.tar.gz
        pack_cmd="tar fcz $arch_name $name"
    else
        pack_cmd="mv $name $arch_name"
    fi

    $pack_cmd

    rm -rf $name

    echo "=> DONE"
    echo
}

rm -rf packs/*
mkdir -p packs
[ -n "$1" ] && {
    proceed $1.conf
} || {
    for repo in $(ls config)
    do proceed $repo
    done
}

